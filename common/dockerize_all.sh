#!/bin/bash

# This script is copied into all docker images.
# It runs the list of scripts named "*dockerize*" in its directory.

MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
ME="$(basename -- $0)"

echo "Running all the dockerize scripts in ${MY_DIR}…"
cd ${MY_DIR}

for SCRIPT in *dockerize*.sh ; do
    [[ "${SCRIPT}" == "${ME}" ]] && continue
    echo "Running ${SCRIPT}…"
    cd ${MY_DIR}
    bash $SCRIPT $@
done

echo "Done running the dockerize scripts!"
