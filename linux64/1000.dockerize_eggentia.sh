#!/bin/bash

echo "Building Essentia"

apt-get --quiet=2 --assume-yes install \
    build-essential \
    git \
    curl \
    cmake \
    yasm \
    libyaml-dev \
    libfftw3-dev \
    libavcodec-dev \
    libavformat-dev \
    libavutil-dev \
    libavresample-dev \
    libsamplerate0-dev \
    libtag1-dev \
    libchromaprint-dev
    

git clone https://github.com/MTG/essentia.git /var/essentia


echo "Building Essentia's dependencies…"

/var/essentia/packaging/build_3rdparty_static_debian.sh

#exit 0

echo "Building Essentia itself…"

cd /var/essentia
./waf configure -v --build-static
./waf -v
./waf install
cd /


echo "Cloning the Eggentia module…"

git clone https://framagit.org/godotrio/godot-module-eggentia.git /var/godot-module-eggentia


echo "Installing the Eggentia module…"

ln -s /var/godot-module-eggentia/eggentia /var/godot/modules/eggentia
