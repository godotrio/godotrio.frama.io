#!/bin/bash

echo "Building Essentia"

apt -y install \
    build-essential \
    libyaml-dev \
    libfftw3-dev \
    libavcodec-dev \
    libavformat-dev \
    libavutil-dev \
    libavresample-dev \
    libsamplerate0-dev \
    libtag1-dev \
    libchromaprint-dev

    
# pwd

git clone https://github.com/MTG/essentia.git /var/essentia

cd /var/essentia
./waf configure -v --build-static
./waf -v

# ls -la build
# ls -la build/src
