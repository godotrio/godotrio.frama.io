#!/bin/bash

# TODO
SERVICE_PREFIX="godot_bakery_"
ALL_TARGETS=("linux64" "linux32" "windows64" "windows32")

TARGETS=("linux64")

MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
ME="$(basename -- $0)"

# Copy some configuration around
for TARGET in ${TARGETS[*]} ; do
    cp .env $TARGET/.env
    cp common/* $TARGET/
done

# Let modules do their things
# for SCRIPT in *prepare*.sh ; do
#     echo "Running preparation script ${SCRIPT}…"
#     cd ${MY_DIR}
#     bash $SCRIPT $@
# done

cd ${MY_DIR}
# docker-compose rm
for TARGET in ${TARGETS[*]} ; do
    echo "Building docker image for $TARGET…"
    docker-compose build --no-cache ${SERVICE_PREFIX}${TARGET}
done



cd ${MY_DIR}
for TARGET in ${TARGETS[*]} ; do
    echo "Running docker container for $TARGET…"
    docker-compose up ${SERVICE_PREFIX}${TARGET}
done


echo "All done. Look for your builds in ./bin/"
echo "> ls bin"
ls -lah bin
